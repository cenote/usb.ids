
usb.ids
--------

This repo is intended to track my own additions to
https://usb-ids.gowdy.us/ and use them by myself before they are
officially added.

From the origin:
The contents of the database and the generated files can be
distributed under the terms of either the GNU General Public License
(version 2 or later) or of the 3-clause BSD License. 
